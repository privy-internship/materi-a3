package main

import (
	"go-mongo/controllers"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
	// perlu dicatat bahwa mgo ini sudah tidak bisa dipakai
	//untuk mongodb versi 5 dan keatas (Lihat versi mongodb pada cluster di MongoDB Atlas ) sehingga perlu menggunakan package mongo
)

func main() {
	// fmt.Print("anything")
	r := httprouter.New()
	uc := controllers.NewUserController(getSession())
	r.GET("/user/:id", uc.GetUser)
	r.POST("/user", uc.CreateUser)
	r.DELETE("/user/:id", uc.DeleteUser)
	http.ListenAndServe("localhost:8080", r) // url perlu ditest
}

func getSession() *mgo.Session {

	s, err := mgo.Dial("mongodb+srv://vicky:ZGod313eoKmTAjIs@cluster0.dgmgjky.mongodb.net/?retryWrites=true&w=majority") // <=== saat ini belum mempunyai url mongodb
	if err != nil {
		panic(err)
	}
	return s

}
