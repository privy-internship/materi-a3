package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"

	"pust-api/handler"
	
)

func main() {

	route := gin.Default()

	v1 := route.Group("/v1")

	v1.GET("/", handler.RootHand)

	v1.GET("/hello", handler.HelloHand)

	v1.GET("/books/:id/:title", handler.BookHand)

	v1.GET("/query", handler.QueryHand)
	v1.POST("/books", postBookHand)

	route.Run()

}



type BookInput struct {
	Title string      `json:"title" binding:"required"`
	Price json.Number `json:"price" binding:"required,number"`
}

func postBookHand(ct *gin.Context) {

	var bookIn BookInput

	err := ct.ShouldBindJSON(&bookIn)
	if err != nil {

		errMes := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMes := fmt.Sprintf("Error on field %s, condition: %s", e.Field(), e.ActualTag())
			errMes = append(errMes, errorMes)
		}
		ct.JSON(http.StatusBadRequest, gin.H{
			"errors": errMes,
		})
		return
	}
	ct.JSON(http.StatusOK, gin.H{
		"title": bookIn.Title,
		"price": bookIn.Price,
	})
}
