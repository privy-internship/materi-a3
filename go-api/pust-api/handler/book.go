package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func RootHand(ct *gin.Context) {
	ct.JSON(http.StatusOK, gin.H{
		"name":          "Vicky Alexander Susanto",
		"favorite-food": "Sushi",
		"buddy":         "Sadewa",
	})
}
func HelloHand(ct *gin.Context) {
	ct.JSON(http.StatusOK, gin.H{
		"greet": "Hello guys",
		"test":  "The test is in progress",
	})
}

func BookHand(ct *gin.Context) {
	id := ct.Param("id")
	title := ct.Param("title")

	ct.JSON(http.StatusOK, gin.H{
		"id":    id,
		"title": title,
	})
}
func QueryHand(ct *gin.Context) {
	title := ct.Query("title")
	price := ct.Query("price")
	ct.JSON(http.StatusOK, gin.H{
		"title": title,
		"price": price,
	})
}
